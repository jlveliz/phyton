import re

nombre1 = "Karen Pacheco"
nombre2 = 'Jorge Veliz'
nombre3 = 'karen Lopez'


#match busca al principio de un string
if re.match('Karen',nombre2,re.IGNORECASE):
    print('Hemos encontrado a ' + nombre2)
else:
    print('No la hemos encontrado')

#busca a todo el string
if re.search('karen',nombre3):
    print('Hemos encontrado a ' + nombre3)
else:
    print('No la hemos encontrado')