import re

listaNombres = [
    'Jorge Veliz',
    'Karen Pachecho',
    'Samantha Veliz',
    'Abel Veliz',
    'Fatima Berzosa',
    'Karen Almeida'
]

for elemento in listaNombres:
    if re.findall('[A-C]$',elemento):
        print(elemento)
