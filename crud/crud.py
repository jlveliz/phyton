from tkinter import *
from tkinter import messagebox
import sqlite3

#---------------------- FUNCIONES -----------------------------
def conectionDBB():
    
    try:
        miconexion = sqlite3.connect("usuarios")
        cursor = miconexion.cursor()
        cursor.execute("""CREATE TABlE DATOSUSUARIOS(
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            NOMBRE_USUARIO VARCHAR(50),
            PASSWORD VARCHAR(50),
            APELLIDO VARCHAR(50),
            DIRECCION VARCHAR(50),
            COMENTARIOS VARCHAR(100)
        )""")
        messagebox.showinfo("DB","BASE DE DATOS CREADA CON EXITO")
    except:
        messagebox.showwarning("DB!","La base de datos ya existe")


def salirAppl():
    prompt = messagebox.askquestion("Esta seguro?")
    if(prompt == 'yes'):
        root.destroy()

def limpiarCampos():
    miId.set("")
    miNombre.set("")
    miApellido.set("")
    miDireccion.set("")
    miPass.set("")
    cuadroComentarios.delete(1.0,END)

#Viene lo PEPA
def crear():
    miConexion = sqlite3.connect("usuarios")
    cursor = miConexion.cursor()
    cursor.execute(" INSERT INTO DATOSUSUARIOS VALUES(NULL,'" + miNombre.get() + "', '"+ miPass.get() +"', '"+ miApellido.get() +"', '"+ miDireccion.get() +"', '"+ cuadroComentarios.get("1.0",END) +"')")
    miConexion.commit()
    messagebox.showinfo("Atención", "Usuario Creado con éxito")

def leer():
    miConexion = sqlite3.connect('usuarios')
    cursor = miConexion.cursor()
    cursor.execute("SELECT * FROM DATOSUSUARIOS WHERE ID= "+ miId.get())
    dataArray = cursor.fetchall()
    
    for item in dataArray:
        miNombre.set( item[1] )
        miPass.set( item[2] )
        miApellido.set( item[3] )
        miDireccion.set( item[4] )
        cuadroComentarios.insert(1.0, item[5] )
    
    miConexion.commit()

def actualizar() :
    miConexion = sqlite3.connect("usuarios")
    cursor = miConexion.cursor()
    cursor.execute(" UPDATE DATOSUSUARIOS SET NOMBRE_USUARIO =  '" + miNombre.get() + "', PASSWORD =  '"+ miPass.get() +"', APELLIDO = '"+ miApellido.get() +"', DIRECCION = '"+ miDireccion.get() +"', COMENTARIOS=  '"+ cuadroComentarios.get("1.0",END) +"' WHERE ID  = '"+ miId.get() +"' "  )
    miConexion.commit()
    messagebox.showinfo("Atención", "Usuario Actualizado con éxito")
    
def eliminar() :
    miConexion = sqlite3.connect("usuarios")
    cursor = miConexion.cursor()  
    cursor.execute("DELETE FROM DATOSUSUARIOS WHERE ID = " + miId.get())
    miConexion.commit()
    messagebox.showinfo("Atención", "Usuario Eliminado con éxito")
    
#--------------------- INTERFAZ GRAFICA------------------------


root = Tk()

#Menu
barraMenu = Menu(root)
root.config(menu=barraMenu,width=300,height=500)

bbddMenu = Menu(barraMenu,tearoff=0)
bbddMenu.add_command(label="Conectar",command=conectionDBB)
bbddMenu.add_command(label="Salir",command=salirAppl)

borrarMenu = Menu(barraMenu,tearoff=0)
borrarMenu.add_command(label="Borrar campos", command=limpiarCampos)

crudMenu = Menu(barraMenu,tearoff=0)
crudMenu.add_command(label="Crear",command=crear)
crudMenu.add_command(label="Leer", command=leer)
crudMenu.add_command(label="Actualizar", command=actualizar)
crudMenu.add_command(label="Borrar",command=eliminar)

ayudaMenu = Menu(barraMenu,tearoff=0)
ayudaMenu.add_command(label="Licencia")
ayudaMenu.add_command(label="Acerca de...")

barraMenu.add_cascade(label="BBDD",menu=bbddMenu)
barraMenu.add_cascade(label="Borrar",menu=borrarMenu)
barraMenu.add_cascade(label="CRUD",menu=crudMenu)
barraMenu.add_cascade(label="Ayuda",menu=ayudaMenu)

#----------------------------- CAMPOS --------------------------·#
miframe = Frame(root)
miframe.pack()


miId = StringVar()
miNombre = StringVar()
miApellido = StringVar()
miPass = StringVar()
miDireccion = StringVar()


cuadroId = Entry(miframe, textvariable=miId)
cuadroId.grid(row=0,column=1,padx=10,pady=10)


cuadroNombre = Entry(miframe, textvariable=miNombre)
cuadroNombre.grid(row=1,column=1,padx=10,pady=10)
cuadroNombre.config(fg="red",justify="right")

cuadroPassword = Entry(miframe, textvariable=miPass)
cuadroPassword.grid(row=2,column=1,padx=10,pady=10)
cuadroPassword.config(show="*")

cuadroApellido = Entry(miframe, textvariable=miApellido)
cuadroApellido.grid(row=3,column=1,padx=10,pady=10)

cuadroDireccion = Entry(miframe, textvariable=miDireccion)
cuadroDireccion.grid(row=4,column=1,padx=10,pady=10)

cuadroComentarios = Text(miframe,width=16, height=5)
cuadroComentarios.grid(row=5,column=1,padx=10,pady=10)
scrolVarComentarios = Scrollbar(miframe,command=cuadroComentarios.yview)
scrolVarComentarios.grid(row=5,column=2, sticky="nsew")
cuadroComentarios.config(yscrollcommand=scrolVarComentarios.set)


#----------------------------- LABELS --------------------------·#
lblId = Label(miframe,text="Id:") 
lblId.grid(row=0,column=0,padx=10,pady=10,sticky="e")

lblNombre = Label(miframe,text="Nombre:") 
lblNombre.grid(row=1,column=0,padx=10,pady=10,sticky="e")

lblPassword = Label(miframe,text="Contraseña:") 
lblPassword.grid(row=2,column=0,padx=10,pady=10,sticky="e")

lblApellido = Label(miframe,text="Apellido:") 
lblApellido.grid(row=3,column=0,padx=10,pady=10,sticky="e")

lblDireccion = Label(miframe,text="Dirección:") 
lblDireccion.grid(row=4,column=0,padx=10,pady=10,sticky="e")

lblComentario = Label(miframe,text="Comentario:") 
lblComentario.grid(row=5,column=0,padx=10,pady=10,sticky="e")

#----------------------------- BOTONES --------------------------·#
frameBotones = Frame(root)
frameBotones.pack()

btnCrear = Button(frameBotones, text="Crear",command=crear)
btnCrear.grid(row=0,column=0,sticky="e",padx=10,pady=10)

btnLeer = Button(frameBotones, text="Leer",command=leer)
btnLeer.grid(row=0,column=1,sticky="e",padx=10,pady=10)

btnActualizar = Button(frameBotones, text="Actualizar", command=actualizar)
btnActualizar.grid(row=0,column=2,sticky="e",padx=10,pady=10)

btnBorrar = Button(frameBotones, text="Borrar", command=eliminar)
btnBorrar.grid(row=0,column=3,sticky="e",padx=10,pady=10)

root.mainloop()