from io import open

#escritura
# archivo = open("prueba.txt","w")
# frase = "que lindo dia para python \nhola hola "

# archivo.write(frase)

# archivo.close()

#lectura
# archivo = open("prueba.txt","r")
# # texto = archivo.read() #devuelve un string
# texto = archivo.readlines() # lee el documento y devuelve un arreglo
# archivo.close()
# for text in texto:
#     print(text)


#añadir lineas
archivo = open("prueba.txt","a")
textoAnadir = "\nUn nuevo texto, te amo karen"
archivo.write(textoAnadir)
archivo.close()