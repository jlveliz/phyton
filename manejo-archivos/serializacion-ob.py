import pickle

class Vehiculo() :
    
    def __init__(self, marca, modelo):
        self.marca = marca
        self.modelo = modelo
        self.enMarcha = False
        self.acelera = False
        self.frena = False

    def arrancar(self):
        self.enMarcha = True

    def acelerar(self):
        self.acelera = True
    
    def frenar(self):
        self.frena = True
    
    def estado(self):
        print("Marca; ", self.marca, "\nModelo: ", self.modelo , "\n En Marcha: ", self.enMarcha, "\n En Marcha: " , self.enMarcha, 
        "\n Acelerando: ", self.acelera, "\n Frena: ", self.frena)

#inserto serializacion
miMoto = Vehiculo("Zusuki", "2020")
miMoto1 = Vehiculo("Zusuki2", "2020")
vehiculos = [miMoto,miMoto1]
fichero = open("serializar-ob","wb")
pickle.dump(vehiculos,fichero)
fichero.close()

#lee serializacion

fichero2 = open("serializar-ob","rb")
objetos = pickle.load(fichero2)

for obj in objetos:
    print(obj.estado())

fichero2.close()