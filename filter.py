#def numero_par(num):
#    if num % 2 == 0:
#        return True
#    else:
#        return False


numeros = [17,24,8,3,5,545,56]

print( list( filter(lambda numero_par:numero_par%2==0,numeros)))



class Empleado:
    def __init__(self, nombre, cargo, salario):
        self.nombre = nombre
        self.cargo = cargo
        self. salario = salario
    
    
    def __str__(self):
        return "{} que trabaja como {} tiene un slario de {} $".format(self.nombre,self.cargo,self.salario)


listaEmpleados = [
    Empleado("Juan", "Director", 124),
    Empleado("Jorge", "Gerente", 45645),
    Empleado("Samantha", "Gerente", 45645),
    Empleado("Karen ", "Gerente", 45645),
]

salarios_altos = filter(lambda empleado: empleado.salario > 500, listaEmpleados)

for empleado in salarios_altos:
    print(empleado)