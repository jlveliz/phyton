#Metodos para manejo de cadenas
#upper() convierte a mayusculas 
#lower() convierte a min
#capitalize() Solo primera letra en mayuscula
#count() cuenta el numero de string en la cadena
#find() busca un elemento
minombre = input("Inserte su nombre: ")
print(minombre.upper())
print(minombre.lower())
print(minombre.capitalize())
print(minombre.isdigit())

edad = input("Inserte la edad: ")

while edad.isdigit() == False:
    print("Por favor, introduce un numero valido")
    edad = input("Introduce una edad: ")

if(int(edad) < 18):
    print("No puede pasar")
else:
    print("Puede pasar")