
# * significa que pueden pasar muchos argumentos
# este ejercicio permite a un generador devolver for anidados
def devuelve_ciudades(*ciudades):
    for ciudad in ciudades:
        #for subEl in ciudad:
            yield from ciudad

ciudades_devuelta = devuelve_ciudades("Madrid","Barcelona", "Guayaquil")

print(next(ciudades_devuelta))
print(next(ciudades_devuelta))