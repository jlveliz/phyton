#decoradora
def funcionDecoradora(funcion):
    def funcionInterna(*args, **kewargs):
        #Acciones adicionales que decoran
        print("Vamos a realizar un calculo: ")
        funcion(*args, **kewargs)
        #agrega mas acciones
        print("Hemos terminado las acciones adicionales")
    
    return funcionInterna








@funcionDecoradora
def suma(num1,num2,num3):
    print(num1 + num2 + num3)

def resta(num1):
    print(30-num1)

@funcionDecoradora
def potencia(base,exponente):
    print(pow(base,exponente))


suma(2,2,2)
resta(10)
potencia(base = 5, exponente = 2)
