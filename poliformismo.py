class Coche() :
    def desplazamiento(self):
        print("Me desplazo utilizando cuatro ruedas")


class Moto():
    def desplazamiento(self):
        print("Me desplazo en dos ruedas")

class Camion():
    def desplazamiento(self):
        print("Me desplazo en 6 ruedas")


miMoto = Moto()
miMoto.desplazamiento()

miCoche = Coche()
miCoche.desplazamiento()