from tkinter import *

raiz = Tk()

miFrame = Frame(raiz, width=1200, height=600,pady=50,padx=50)
miFrame.pack()

#se declara que es una variable string
miNombre = StringVar()

cuadroNombre = Entry(miFrame,textvariable=miNombre)
cuadroNombre.grid(row=0,column=1)
cuadroNombre.focus()

cuadroApellido = Entry(miFrame)
cuadroApellido.grid(row=1,column=1)

cuadroDireccion = Entry(miFrame)
cuadroDireccion.grid(row=2,column=1)

cuadroPass = Entry(miFrame)
cuadroPass.grid(row=3,column=1)
cuadroPass.config(show="*")

comentarioTexto = Text(miFrame, width=16,height=5)
comentarioTexto.grid(row=4,column=1)

scrollVert = Scrollbar(miFrame, command=comentarioTexto.yview)
scrollVert.grid(row=4,column=2, sticky="nsew")

comentarioTexto.config(yscrollcommand=scrollVert.set)


nombreLabel = Label(miFrame,text="Nombre: ")
nombreLabel.grid(row=0, column=0,sticky="w")
apellidoLabel = Label(miFrame,text="Apellido: ")
apellidoLabel.grid(row=1, column=0,sticky="w")

direccionLabel = Label(miFrame,text="Direccion: ")
direccionLabel.grid(row=2, column=0,sticky="w")

passLabel = Label(miFrame,text="Contraseña: ")
passLabel.grid(row=3, column=0,sticky="w")


comentarioLabel = Label(miFrame,text="Comentario: ")
comentarioLabel.grid(row=4, column=0,sticky="w")

def codigoBoton():
    miNombre.set("Juan")


btnEnvio = Button(miFrame, text="Enviar", command=codigoBoton)
btnEnvio.grid(row=5,column=1)


raiz.mainloop()