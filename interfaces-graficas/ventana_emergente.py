from tkinter import *
from tkinter import messagebox


root = Tk()

def infoAdicional():
    messagebox.showinfo("Procesador de Juan", "Procesador de Textos")

def avisoLicencia():
    messagebox.showwarning("Licencia", "Producto bajo licencia")

def avisoAdvertenciaSalida():
    #valor = messagebox.askquestion("Salir", "Está usted seguro?")
    valor = messagebox.askyesnocancel("Salir", "Desea Salir?")
    if(valor == 'yes'):
        root.destroy()



button = Button(root,text="Texto", command=infoAdicional)
buttonLicencia = Button(root,text="Texto Licencia", command=avisoLicencia)
buttonSalir = Button(root,text="Salir", command=avisoAdvertenciaSalida)

button.pack()
buttonLicencia.pack()
buttonSalir.pack()

root.mainloop()