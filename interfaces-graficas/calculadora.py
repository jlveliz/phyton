from tkinter import *

raiz = Tk()

miFrame = Frame(raiz)
miFrame.pack()

numeroPantalla = StringVar()
operacion = ""
resultado = 0


def numPulsado(num):
    global operacion
    if operacion != "":
        numeroPantalla.set(num)
        operacion = ""
    else:
        valorAlmacenado = numeroPantalla.get()
        numeroPantalla.set(valorAlmacenado + num)

def sumar(num):
    global operacion
    global resultado
    resultado+=int(num)
    numeroPantalla.set(resultado)
    operacion = "suma"

def elResultado():
    global resultado
    numeroPantalla.set(resultado +  int(numeroPantalla.get()))
    resultado = 0


#pantalla calculadora
pantallaCalc = Entry(miFrame, textvariable=numeroPantalla)
pantallaCalc.grid(row=1,column=1,padx=10, pady=10, columnspan=4)
pantallaCalc.config(background="black",fg="#03f943",justify="right")


#primera fila de botones
btnSeven = Button(miFrame,text="7", width=3, command=lambda:numPulsado("7"))
btnSeven.grid(row=2,column=1)
btnEight = Button(miFrame,text="8", width=3, command=lambda:numPulsado("8"))
btnEight.grid(row=2,column=2)
btnNine = Button(miFrame,text="9", width=3, command=lambda:numPulsado("9"))
btnNine.grid(row=2,column=3)
btnDiv = Button(miFrame,text="/", width=3)
btnDiv.grid(row=2,column=4)

#segunda fila de botones
btnFour = Button(miFrame,text="4", width=3, command=lambda:numPulsado("4"))
btnFour.grid(row=3,column=1)
btnFive = Button(miFrame,text="5", width=3, command=lambda:numPulsado("5"))
btnFive.grid(row=3,column=2)
btnSix = Button(miFrame,text="6", width=3, command=lambda:numPulsado("6"))
btnSix.grid(row=3,column=3)
btnMult = Button(miFrame,text="x", width=3)
btnMult.grid(row=3,column=4)

#tecera fila de botones
btnOne = Button(miFrame,text="1", width=3, command=lambda:numPulsado("1"))
btnOne.grid(row=4,column=1)
btnTwo = Button(miFrame,text="2", width=3, command=lambda:numPulsado("2"))
btnTwo.grid(row=4,column=2)
btnThree = Button(miFrame,text="3", width=3, command=lambda:numPulsado("3"))
btnThree.grid(row=4,column=3)
btnRes = Button(miFrame,text="-", width=3)
btnRes.grid(row=4,column=4)


#cuarta fila de botones
btnZero = Button(miFrame,text="0", width=3, command=lambda:numPulsado("0"))
btnZero.grid(row=5,column=1)
btnComa = Button(miFrame,text=",", width=3, command=lambda:numPulsado(","))
btnComa.grid(row=5,column=2)
btnEqual = Button(miFrame,text="=", width=3,command=lambda:elResultado())
btnEqual.grid(row=5,column=3)
btnSum = Button(miFrame,text="+", width=3, command=lambda:sumar(numeroPantalla.get()))
btnSum.grid(row=5,column=4)



raiz.mainloop()