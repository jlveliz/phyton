import sqlite3

myConnect = sqlite3.connect("PrimerBase")

cursor = myConnect.cursor()

#cursor.execute("CREATE TABLE PRODUCTOS (NOMBRE_ARTICULO VARCHAR(50), PRECIO INTEGER, SECCION VARCHAR(20) )")
#cursor.execute("INSERT INTO PRODUCTOS VALUES('BALON', 20, 'Deportes'); ")

#variosProductos = [
#    ("Camisera", 10, "Deportes"),
#    ("Jarron", 90, "Ceramica"),
#    ("Camion para niño", 20, "Jugueteria")
#]

#cursor.executemany("INSERT INTO PRODUCTOS VALUES(?,?,?)",variosProductos)


cursor.execute("SELECT * FROM PRODUCTOS")

variosProductos = cursor.fetchall()

for producto in variosProductos:
    print(producto)


myConnect.commit()

myConnect.close()