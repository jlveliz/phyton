import pickle

class Persona:

    def __init__(self, nombre, genero, edad):
        self.nombre = nombre
        self.genero = genero
        self.edad = edad
        print("se ha creado una persona nueva con el nombre de: ", self.nombre)
    
    # convierte en cadena de texto la info del obj
    def __str__(self):
        return "{} {} {}".format(self.nombre,self.genero,self.edad)

class ListaPersona:
    personas = []

    def __init__(self):
        listaPersonas = open("fichero_personas.txt","ab+")
        listaPersonas.seek(0)
        try:
            self.personas = pickle.load(listaPersonas)
            print("Se cargaron {} personas del fichero externo".format(len(self.personas)))
        except:
            print("El fichero está vacio")
        finally:
            listaPersonas.close()
            del(listaPersonas)

    def agregarPersona(self, p):
        self.personas.append(p)
        self.guardarPersonasEnFicheroExterno()


    def mostrarPersonas(self):
        for persona in self.personas:
            print(persona)
    
    def guardarPersonasEnFicheroExterno(self):
        listaPersonas = open("fichero_personas.txt","wb")
        pickle.dump(self.personas,listaPersonas)
        del(listaPersonas)
    
    def mostrarInfoFicheroExterno(self):
        print("La Informacion del fichero externo es la siguiente:")
        for persona in self.personas:
            print(persona)


lista = ListaPersona()
persona = Persona("Antonio", "Masculino", 24)
lista.agregarPersona(persona)
lista.mostrarInfoFicheroExterno()

