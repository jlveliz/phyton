#permite almacenar varios tipos de datos

#crea uan asociacion clave:valor oara cada elemento almacenado

#los elementos almacenados no están ordenados

miDiccionario = {
    "Alemania" : "Berlin",
    "Francia" : "París",
    "Reino Unido" : "Londres",
    "España" : "Madrid"
}

capitalFrancia = miDiccionario["Francia"]
print(capitalFrancia)

#agregar Elemento o actualizar
miDiccionario["Japon"] = "Pekin"

print(miDiccionario)

miDiccionario["Japon"] = "Tokyo"
print(miDiccionario)

#eliminar elemento
del miDiccionario["Japon"]

print(miDiccionario)

#keys : devuelve las keys de un diccionario
print(miDiccionario.keys())

#values : devuelve los valores de un diccionario
print(miDiccionario.values())

#length : devuelve la longitud de un diccionario
print(len(miDiccionario))