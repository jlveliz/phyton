miLista = [1,2,3]

#toda la lista
print(miLista)
#podemos de ir de atrás hacia adelante
print(miLista[-3])

#porcion de lista
print(miLista[0:2]) 
print(miLista[:3])

#agregar un elemento: primer parametro se añade el indice que desea agregar y segundo 
# parametro el elemento  en la lista
miLista.insert(2,10)
print(miLista)

#agregar muchos elementos
miLista.extend(["Jorge","Karen","Samantha","Jorge"])
print(miLista)

#get index de una lista
idx = miLista.index("Jorge")
print(idx)

#comprobar si un elemento existe en una lista
seEncuentra = "Jorge" in miLista
print(seEncuentra)

#eliminar elemento 
miLista.remove("Jorge")
print(miLista)

#elimina ultimo elemento de lista o un indice
miLista.pop()
print(miLista)
