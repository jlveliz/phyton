# Son estructuras que extraen valores de una funcion 
# y se almacenan en objetos iterables ( se pueden recorrer )

# Crear un programa con un generador de numeros
def generaNumerosPares(limite):
    num = 1
    miLista =[]

    while num < limite:
        #construye una lista iterable para poder ser recorrido
        yield num*2
        num = num + 1
    

numPares = generaNumerosPares(10)

print(next(numPares))
print("Aqui more code")
print(next(numPares))
print("Aqui more code")
print(next(numPares))