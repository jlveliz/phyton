class Coche():

    def __init__(self) :
        self.largoChasis = 250
        self.anchoChasis = 120
        self.__ruedas = 4 #encapzulado es decir es una propiedad privada
        self.enMarcha = False

    def __chequeoInterno(self): #método encapsulado o privado
        print("Realizando Chequeo Interno")
        self.gasolina = "ok"
        self.aceite ="ok"
        self.puertas = "cerradas"
        if(self.gasolina == 'ok' and self.aceite == 'ok' and self.puertas == "cerradas"):
            return True
        else:
            return False
        

    

    def arrancar(self):
        if(self.enMarcha == False and self.chequeoInterno == True):
            print("El Coche ha arrancado")
        elif(self.enMarcha and self.chequeoInterno()):
            self.enMarcha = True
            print("El Coche ha arrancado")
    
    def estado(self):
        if(self.enMarcha):
            return "El Coche está en Marcha"
        else:
            return "El Coche está detenido"

miCoche = Coche()
print("EL coche tiene " , miCoche.__ruedas , " ruedas")
print(miCoche.arrancar())
print(miCoche.estado())

print("------------- A Continuacion creamos el segundo objeto-----------------")
otroCoche = Coche()
#otroCoche.__ruedas = 3
print("El otro coche tiene " , otroCoche.__ruedas , " ruedas")
print(otroCoche.estado())
