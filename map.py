class Empleado:
    def __init__(self, nombre, cargo, salario):
        self.nombre = nombre
        self.cargo = cargo
        self. salario = salario
    
    
    def __str__(self):
        return "{} que trabaja como {} tiene un slario de {} $".format(self.nombre,self.cargo,self.salario)


listaEmpleados = [
    Empleado("Juan", "Director", 124),
    Empleado("Jorge", "Gerente", 45645),
    Empleado("Samantha", "Gerente", 45645),
    Empleado("Karen ", "Gerente", 45645),
]


def calculo_comision(empleado):
    empleado.salario = empleado.salario * 1.03
    return empleado

empleadosComision = map(calculo_comision,listaEmpleados)

for empleado in empleadosComision:
    print(empleado)