def area_triangulo(base, altura):
    """ 
        Permite calcular el area de un triangulo dado 
        >>> area_triangulo(3,6)
        9.0
    """
    return (base * altura)  / 2


import doctest

doctest.testmod()