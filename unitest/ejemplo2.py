def verificaEmail(email):
    """
        Evalua un email recibido en busca del @. si tiene un @ 
        es correcto, si tiene mas de un @ es incorrecto
        
        >>> verificaEmail('jorge.veliz@guayas.gob.ec')
        True
        
        >>> verificaEmail('jorge.velizguayas.gob.ec@')
        False
        
        >>> verificaEmail('jorge.veliz')
        False
        
    """
    arroba = email.count("@")
    if (arroba != 0 or email.rfind("@") == (len(email)-1) or email.find('@') == 0) : 
        return False
    else:
        return True



import doctest

doctest.testmod()