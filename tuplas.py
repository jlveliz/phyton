# son listas inmutables
    # no se pueden añadir, eliminar o mover elementos 
    # permite extraer porciones, al extraer una tupla se forma otra tupla
    # no permite busquedas
    # permite comprobar si un elemento se encuentra en la tupla

# Utilidades:
    # más rápidas
    # menos espacio en memoria
    # permiten formatear strings
    # pueden utilizarse como claves de un diccinoario (?)

miTupla = ("Karen", 20, "Jorge")

print(miTupla[1])

#convertir tupla en lista
tuplaTolist =  list(miTupla)
print(tuplaTolist)

#convertir una lista en una tupla
listToTupla = tuple(tuplaTolist)
print(listToTupla)

#saber si un elemento está en la tupla
print("Juan" in miTupla)

#saber cuantas veces se repite un elemento elementos forman parte de la tupla
conteoTupla = miTupla.count("Jorge")
print(conteoTupla)

#saber la longitud de una tupla
print(len(miTupla))

#tuplas unitarias
otraTupla = ("Juan",)
print(len(otraTupla))

#otra forma de crear tuplas (enpaquetando tuplas)
tuplaSinParentesis = "Juan", 13, 1 , "Karen"
#desempaquetado de tuplas
nombre, numero, numero1 = tuplaSinParentesis
print (nombre)
print (numero)
print (numero1)

