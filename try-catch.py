def suma(num1, num2):
    return num1 + num2

def resta(num1, num2):
    return num1 - num2

def multiplica(num1, num2):
    return num1 * num2

def divide(num1, num2):
    try:
        return num1 / num2
    except ZeroDivisionError:
        print("No se puede dividir para 0")
        return "Operacion Erronea"

while True:
        try:
            numeroUno = int(input("Introduzca el primer numero: "))
            numeroDos = int(input("Introduzca el segundo numero: "))
            break
        except ValueError:
            print("Tipo de dato no válido")
           



operacion = input("Introduzca la operación a realizar: (suma, resta, multiplicacion, division)")

if operacion == 'suma':
    print(suma(numeroUno,numeroDos))
elif operacion == "resta":
    print(resta(numeroUno,numeroDos))
elif operacion == "multiplicacion":
    print(multiplica(numeroUno,numeroDos))
else:
    print(divide(numeroUno,numeroDos))