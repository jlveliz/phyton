# contador = 0
# miEmail = input("Ingrese el correo electronico: ")

# for i in miEmail:
#     if i == "@" or i == '.' :
#         contador = contador + 1

# if(contador >= 2):
#     print("Email es correcto")
# else:
#     print("No lo es")

# primer y segundo parámetro desde que indice a que indice va a llegar
# tercer parámetro de cuantos en cuantos va a ir haciendo el indice de inicio y fin
# for i in range(5,10,3):
#     # f se hace una notación de concatenacion para unir texto con variables
#     print(f"valor de la variable {i}")

valido = False
email = input("Introduce tu correo ")
for i in range(len(email)):
    if(email[i] == "@"):
        valido = True

if valido:
    print("Email correcto")
else:
    print("Email incorrecto")