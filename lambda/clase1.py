# def area_triangulo(base, altura):
#    return (base * altura) / 2

#triangulo = area_triangulo(2,2)

#print(triangulo)

# segunda forma de hacer la funcion, por medio de lambda
#no puede tener condicionales o iteraciones
area_triangulo = lambda base,altura: (base * altura) / 2

triangulo = area_triangulo(5,3)
print(triangulo)