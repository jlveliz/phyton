class Persona():
    def __init__(self, nombre, edad, lugar_nacimiento):
        self.nombre = nombre
        self.edad = edad
        self.lugar_nacimiento = lugar_nacimiento

    def descripcion(self):
        print("Nombre: ", self.nombre, " Edad: ", self.edad, ", Lugar de Nacimiento: ", self.lugar_nacimiento)

class Empleado(Persona):

    def __init__(self, salario, antiguedad, nombre, edad, residencia):
        super().__init__(nombre,edad,residencia)
        self.salario = salario
        self.antiguedad = antiguedad

    def descripcion(self):
        super().descripcion()
        print("Salario: ", self.salario, " Antiguedad: ", self.antiguedad)

karen = Empleado(15, 20,  "Karen", 24, "Guayaquil")
karen.descripcion()
#clases de herencia
esPersona = isinstance(karen,Persona)
print(esPersona)

