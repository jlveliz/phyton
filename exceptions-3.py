import math

def calculaRaiz(num):
    if num < 0 :
        raise ValueError("El numero no puede ser negativo")
    else :
        return math.sqrt(num)


op1 = (int(input("Introduce un número: ")))
print(calculaRaiz(op1))


def evaluaEdad(edad):

    if(edad < 0):
        #se genera un exception
        raise TypeError("Edad inválida")

    if edad < 20:
        return "eres muy joven"
    elif edad < 40:
        return "Eres Joven"
    elif edad < 65: 
        return "eres Maduro"
    elif edad < 100:
        return "Cuídate"

print(evaluaEdad(30))
