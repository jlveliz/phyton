class Vehiculo() :
    
    def __init__(self, marca, modelo):
        self.marca = marca
        self.modelo = modelo
        self.enMarcha = False
        self.acelera = False
        self.frena = False

    def arrancar(self):
        self.enMarcha = True

    def acelerar(self):
        self.acelera = True
    
    def frenar(self):
        self.frena = True
    
    def estado(self):
        print("Marca; ", self.marca, "\nModelo: ", self.modelo , "\n En Marcha: ", self.enMarcha, "\n En Marcha: " , self.enMarcha, 
        "\n Acelerando: ", self.acelera, "\n Frena: ", self.frena)

#Asi se hereda
class Moto(Vehiculo):
    pass

miMoto = Moto("Zusuki", "2020")
miMoto.estado()