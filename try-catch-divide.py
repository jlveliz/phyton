def divide():

    try:
        op1 = (float(input("Introduzca el primer numero ")))
        op2 = (float(input("Introduzca el segundo numero ")))

        print("la division es: " + str(op1/op2))
        
    except ValueError:
        print("EL Valor Introducido es erroneo")
    except ZeroDivisionError:
        print("No se puede dividir entre 0")
    
    finally:
        print("Siempre me voy a ejecutar")

divide()